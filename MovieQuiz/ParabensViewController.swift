//
//  ParabensViewController.swift
//  MovieQuiz
//
//  Created by Paulo Gutemberg on 01/05/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class ParabensViewController: UIViewController {

    @IBOutlet weak var lbScore: UILabel!
    var score: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        lbScore.text = String(score)
    }
    
    @IBAction func playAgain(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
