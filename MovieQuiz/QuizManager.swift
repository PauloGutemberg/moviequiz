//
//  QuizManager.swift
//  MovieQuiz
//
//  Created by Paulo Gutemberg on 07/05/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

typealias Round = (quiz: Quiz, options: [QuizOption])

class QuizManager {
    var quizes: [Quiz]
    var quizOption: [QuizOption]
    var score: Int
    var round: Round?
    init() {
        score = 0
        let quizesURL = Bundle.main.url(forResource: "quizes", withExtension: "json")!
        do {
           let quizesData = try Data(contentsOf: quizesURL)
            quizes = try JSONDecoder().decode([Quiz].self, from: quizesData)
            
            let quizOptionURL = Bundle.main.url(forResource: "options", withExtension: "json")!
            let quizOptionsData = try! Data(contentsOf: quizOptionURL)
            quizOption = try JSONDecoder().decode([QuizOption].self, from: quizOptionsData)
        } catch let erro {
            print(erro)
            quizes = []
            quizOption = []
        }
    }
    
    func generateRandomQuiz()-> Round{
        let quizIndex = Int(arc4random_uniform(UInt32(quizes.count)))
        let quiz = quizes[quizIndex]
        var indexes: Set<Int> = [quizIndex]
        while indexes.count < 4 {
            let index = Int(arc4random_uniform(UInt32(quizes.count)))
            indexes.insert(index)
        }
        let options = indexes.map({
            quizOption[$0]
        })
        round = (quiz, options)
        return round!
    }
    
    func checkAnswer(_ answer: String){
        guard let round = round else {return}
        if(answer == round.quiz.name){
            score = score + 1
            print("Acertou! :D")
        }
    }
    
}

struct Quiz: Codable {
    
    let name: String
    let number: Int
    
}


struct QuizOption: Codable {
    let name: String
}
