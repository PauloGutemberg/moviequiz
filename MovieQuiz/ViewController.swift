//
//  ViewController.swift
//  MovieQuiz
//
//  Created by Paulo Gutemberg on 06/04/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var vwSoundBar: UIView!
    @IBOutlet weak var slMusic: UISlider!
    
    @IBOutlet var btnOptions: [UIButton]!
    @IBOutlet weak var ivQuiz: UIImageView!
    
    var quizManager: QuizManager!
    //para musicas localmente no bundle
    var quizPlayer: AVAudioPlayer!
    
    //para streaming de musica ou video
    var playerItem: AVPlayerItem!
    var backGroundMusicPlayer: AVPlayer!
    
    @IBOutlet weak var viTimer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playBackgroundMusic()
        self.vwSoundBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        quizManager = QuizManager()
        getNewQuiz()
        startTimer()
    }
    
    func playBackgroundMusic(){
        let musicURL = Bundle.main.url(forResource: "MarchaImperial", withExtension: ".mp3")!
        playerItem = AVPlayerItem(url: musicURL)
        backGroundMusicPlayer = AVPlayer(playerItem: playerItem)
        backGroundMusicPlayer.volume = 0.1
        backGroundMusicPlayer.play()
        let cmTime = CMTimeMakeWithSeconds(1, preferredTimescale: 1)
        backGroundMusicPlayer.addPeriodicTimeObserver(forInterval: cmTime, queue: nil) { (time) in
            let percent = time.seconds / self.playerItem.duration.seconds
            self.slMusic.setValue(Float(percent), animated: true)
        }
    }
    
    func startTimer(){
        viTimer.frame = self.view.frame
        UIView.animate(withDuration: 60.0, delay: 0.0, options: .curveLinear, animations: {
            self.viTimer.frame.size.width = 0 // ele quer que chege a 0 a largura da view
            self.viTimer.frame.origin.x = self.view.center.x // dentro do eixo de x
        }) { (success) in
            self.gameOver()
        }
    }
    
    func gameOver(){
        quizPlayer.stop()
        performSegue(withIdentifier: "segueParabens", sender: nil)
    }
    
    @IBAction func playQuiz(){
        guard let round = quizManager.round else {
            return
        }
        ivQuiz.image = UIImage(named: "movieSound")
        if let url = Bundle.main.url(forResource: "quote\(round.quiz.number)", withExtension: ".mp3"){
            do {
                //instancia do audio
                self.quizPlayer = try AVAudioPlayer(contentsOf: url)
                // delegando o audio
                self.quizPlayer.delegate = self // precisa ser informado quando o som acaba e ele informa via delegate
                //configuracoes do audio
                self.quizPlayer.volume = 1 //o volume maximo é igual a 1
                self.quizPlayer.play() //tocar
            } catch let errou {
                print(errou)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ParabensViewController
        vc.score = quizManager.score
    }
    
    func getNewQuiz(){
        let round = quizManager.generateRandomQuiz()
        for aux in 0..<round.options.count {
            btnOptions[aux].setTitle(round.options[aux].name, for: .normal)
        }
        playQuiz()
    }
    

    @IBAction func showHideSoundBar(_ sender: Any) {
        vwSoundBar.isHidden = !vwSoundBar.isHidden
    }
    
    @IBAction func changeMusicTime(_ sender: UISlider) {
        let cmTime = CMTimeMakeWithSeconds(Double(sender.value) * playerItem.duration.seconds, preferredTimescale: 1)
        backGroundMusicPlayer.seek(to: cmTime)
    }
    
    @IBAction func changeMusicStatus(_ sender: UIButton) {
        if backGroundMusicPlayer.timeControlStatus == .paused {
            backGroundMusicPlayer.play()
            sender.setImage(UIImage(named: "pause"), for: .normal)
        }  else {
            backGroundMusicPlayer.pause()
            sender.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    @IBAction func checkAnswer(_ sender: UIButton) {
        quizManager.checkAnswer(sender.title(for: .normal)!)
        getNewQuiz()
    }
}

extension ViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        ivQuiz.image = UIImage(named: "movieSoundPause")
    }
}
